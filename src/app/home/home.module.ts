import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutModule } from './../layout/layout.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { BrandComponent } from './components/brand/brand.component';
import { PartnerComponent } from './components/partner/partner.component';
import { HiringComponent } from './components/hiring/hiring.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { HtwComponent } from './components/htw/htw.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { TestimonialComponent } from './components/testimonial/testimonial.component';

@NgModule({
  declarations: [HomeComponent, BrandComponent, PartnerComponent, HiringComponent, CategoriesComponent, HtwComponent, PricingComponent, TestimonialComponent],
  imports: [
    LayoutModule,
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
