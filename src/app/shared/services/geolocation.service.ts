import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';

declare const google: any;

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {
  public lat;
  public lng;

  constructor(
    private mapsAPILoader: MapsAPILoader
  ) { }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          console.log(this.lat);
          console.log(this.lat);
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }
}
