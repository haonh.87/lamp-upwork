import { Component, OnInit } from '@angular/core';
import {AppConstants} from "../../configs/constants";

@Component({
  selector: 'custom-search',
  templateUrl: './custom-search.component.html',
  styleUrls: ['./custom-search.component.scss'],
})
export class CustomSearchComponent implements OnInit  {

  type_search: any = [];

  ngOnInit() {
    this.type_search = AppConstants.TYPE_SEARCH;
  }
}
