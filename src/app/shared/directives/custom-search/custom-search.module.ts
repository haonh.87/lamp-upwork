import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomSearchComponent } from './custom-search.component';

@NgModule({
  declarations: [CustomSearchComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CustomSearchComponent]
})
export class CustomSearchModule { }
