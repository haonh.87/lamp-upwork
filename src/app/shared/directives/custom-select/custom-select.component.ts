import { Component, OnInit, forwardRef, Input, EventEmitter, Output, HostListener, OnChanges, AfterViewInit, ViewChildren, QueryList, ElementRef, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'custom-list',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomSelectComponent),
    multi: true
  }],
})
export class CustomSelectComponent implements OnInit, OnChanges, AfterViewInit  {

  @Input()
  public _value: any;

  @Input()
  public options: any;

  @Input()
  public config: any = {};

  @Input()
  public multiple: boolean = false;

  @Input()
  public disabled: boolean;

  @Output()
  public change: EventEmitter<any> = new EventEmitter();

  @Output()
  public open: EventEmitter<any> = new EventEmitter();

  @Output()
  public close: EventEmitter<any> = new EventEmitter();

  public toggleDropdown: boolean = false;

  public availableItems: any = [];

  public selectedItems: any = [];

  public selectedDisplayText: string = "Select";

  public searchText: string;

  public clickedInside: boolean = false;

  public insideKeyPress: boolean = false;

  public focusedItemIndex: number = null;

  public showNotFound = false;

  @ViewChildren('availableOption') public availableOptions: QueryList<ElementRef>;

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }


  constructor(
    private cdref: ChangeDetectorRef,
    public elementRef: ElementRef
  ) { 
    this.multiple = false;
  }

  public onChange: any = () => {}
  public onTouched: any = () => {}

  @HostListener('click')
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener('document:click')
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.toggleDropdown = false;
      this.resetArrowKeyActiveElement();
    }
    this.clickedInside = false;
  }

  @HostListener('document:keydown')
  public KeyPressOutsideComponent() {
    if (!this.insideKeyPress) {
      this.toggleDropdown = false;
      this.resetArrowKeyActiveElement();
    }
    this.insideKeyPress = false;
  }

  @HostListener('keydown', ['$event'])
  public handleKeyboardEvent($event: KeyboardEvent) {
    this.insideKeyPress = true;
    if ($event.keyCode === 27 || this.disabled) {
      this.toggleDropdown = false;
      this.insideKeyPress = false;
      return;
    }
    const avaOpts = this.availableOptions.toArray();
    if (avaOpts.length === 0 && !this.toggleDropdown) {
      this.toggleDropdown = true;
    }
    // Arrow Down
    if ($event.keyCode === 40 && avaOpts.length > 0) {
      this.onArrowKeyDown();
      avaOpts[this.focusedItemIndex].nativeElement.focus();
      $event.preventDefault();
    }
    // Arrow Up
    if ($event.keyCode === 38 && avaOpts.length) {
      this.onArrowKeyUp();
      avaOpts[this.focusedItemIndex].nativeElement.focus();
      $event.preventDefault();
    }
    // Enter
    if ($event.keyCode === 13 && this.focusedItemIndex !== null) {
      this.selectItem(this.availableItems[this.focusedItemIndex], this.focusedItemIndex);
      return false;
    }
  }

  public ngOnInit() {
    if (typeof this.options !== "undefined" && Array.isArray(this.options)) {
      this.availableItems = [...this.options.sort(this.config.customComparator)];
      this.initDropdownValuesAndOptions();
    }
  }

  public ngAfterViewInit() {
    this.availableOptions.changes.subscribe(this.setNotFoundState.bind(this));
  }

  public ngOnChanges(changes: SimpleChanges) {
    this.selectedItems = [];
    this.searchText = null;
    this.options = this.options || [];
    if (changes.options) {
      this.availableItems = [...this.options.sort(this.config.customComparator)];
    }
    if (changes.value && JSON.stringify(changes.value.currentValue) === JSON.stringify([])) {
      this.availableItems = [...this.options.sort(this.config.customComparator)];
    }
    this.initDropdownValuesAndOptions();
  }

  public registerOnChange(fn: any) {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any) {
    this.onTouched = fn;
  }


  public writeValue(value: any) {
    if (value) {
      if (Array.isArray(value)) {
        if (this.multiple) {
          this.value = value;
        } else {
          this.value = value[0];
        }
      } else {
        this.value = value;
      }
      if (this.selectedItems.length === 0) {
        if (Array.isArray(value)) {
          this.selectedItems = value;
        } else {
          this.selectedItems.push(value);
        }
        this.initDropdownValuesAndOptions();
      }
    }
  }

  public setNotFoundState() {
    if (this.availableOptions.length === 0) {
      this.showNotFound = true;
    } else {
      this.showNotFound = false;
    }
    this.cdref.detectChanges();
  }

  public deselectItem(item: any, index: number) {
    this.selectedItems.splice(index, 1);
    if (!this.availableItems.includes(item)) {
      this.availableItems.push(item);
      this.availableItems.sort(this.config.customComparator);
    }
    this.selectedItems = [...this.selectedItems];
    this.availableItems = [...this.availableItems];
    this.valueChanged();
    this.resetArrowKeyActiveElement();
  }

  public selectItem(item: string, index?: number) {
    if (!this.multiple) {
      if (this.selectedItems.length > 0) {
        this.availableItems.push(this.selectedItems[0]);
      }
      this.selectedItems = [];
      this.toggleDropdown = false;
    }
    this.availableItems.splice(index, 1);
    this.selectedItems.push(item);
    this.selectedItems = [...this.selectedItems];
    this.availableItems = [...this.availableItems];
    this.selectedItems.sort(this.config.customComparator);
    this.availableItems.sort(this.config.customComparator);
    
    this.valueChanged();
    this.resetArrowKeyActiveElement();
  }

  public valueChanged() {
    this.writeValue(this.selectedItems);
    
    this.change.emit({ value: this.value });
    this.setSelectedDisplayText();
  }

  public toggleSelectDropdown() {
    this.toggleDropdown = !this.toggleDropdown;
    if (this.toggleDropdown) {
      this.open.emit();
    } else {
      this.close.emit();
    }
    this.resetArrowKeyActiveElement();
  }

  private resetArrowKeyActiveElement() {
    this.focusedItemIndex = null;
  }

  private onArrowKeyDown() {
    if (this.focusedItemIndex === this.availableItems.length - 1) {
      this.focusedItemIndex = 0;
      return;
    }
    if (this.onArrowKey()) {
      this.focusedItemIndex++;
    }
  }

  private onArrowKeyUp() {
    if (this.focusedItemIndex === 0) {
      this.focusedItemIndex = this.availableItems.length - 1;
      return;
    }
    if (this.onArrowKey()) {
      this.focusedItemIndex--;
    }
  }

  private onArrowKey() {
    if (this.focusedItemIndex === null) {
      this.focusedItemIndex = 0;
      return false;
    }
    return true;
  }

  private initDropdownValuesAndOptions() {
    const config: any = {
      displayKey: "description",
      height: 'auto',
      search: false,
      icon: '',
      placeholder: 'Select',
      searchPlaceholder: 'Search',
      limitTo: this.options.length,
      customComparator: undefined,
      noResultsFound: 'No results found!',
      moreText: 'more',
      searchOnKey: null
    };

    if (this.config === "undefined" || Object.keys(this.config).length === 0) {
      this.config = {...config };
    }

    for (const key of Object.keys(config)) {
      this.config[key] = this.config[key] ? this.config[key] : config[key];
    }

    // adding placeholder in config as default param
    this.selectedDisplayText = this.config['placeholder'];
    if (this.value !== "" && typeof this.value !== "undefined") {
      if (Array.isArray(this.value)) {
        this.selectedItems = this.value;
      } else {
        this.selectedItems[0] = this.value;
      }

      this.selectedItems.forEach((item: any) => {
        const ind = this.availableItems.findIndex((aItem: any) => JSON.stringify(item) === JSON.stringify(aItem));
        if (ind !== -1) {
          this.availableItems.splice(ind, 1);
        }
      });
    }
    this.setSelectedDisplayText();
  }

  private setSelectedDisplayText() {
    let text: string = this.selectedItems[0];
    if (typeof this.selectedItems[0] === "object") {
      text = this.selectedItems[0][this.config.displayKey];
    }

    if (this.multiple && this.selectedItems.length > 0) {
      this.selectedDisplayText = this.selectedItems.length === 1 ? text : text + ` + ${this.selectedItems.length -1} ${this.config.moreText}`;
    } else {
      this.selectedDisplayText = this.selectedItems.length === 0 ? this.config.placeholder : text;
    }
  }
}
