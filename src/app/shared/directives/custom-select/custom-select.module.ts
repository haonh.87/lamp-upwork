import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArrayFilterPipe } from './../../pipes/filter-by.pipe';
import { LimitToPipe } from './../../pipes/limit-to.pipe';
import { CustomSelectComponent } from './custom-select.component';

@NgModule({
  declarations: [CustomSelectComponent, LimitToPipe, ArrayFilterPipe],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CustomSelectComponent, LimitToPipe]
})
export class CustomSelectModule { }
