import {HttpClient, HttpHeaders} from "@angular/common/http";

export function createCommonHeaders(authService) {
  if (!authService.token) {

  }
  const HEADER_PREFIX_BEABER = `t3dfsg45dfg3i5h3hv2gv`;
  let hObject = new HttpHeaders().set("Authorization", HEADER_PREFIX_BEABER + authService.token);

  return hObject;
}

export function extractData(res: Response) {
  let body: any = {};
  try {
    body = res.json();
  } catch (e) {
    console.log(e);
  }

  return body || {};
}

export function extractDataArray(res: Response) {
  let body: any[] = [];
  try {
    // body = res.json();
  } catch (e) {
    console.log(e);
  }

  return body || [];
}
