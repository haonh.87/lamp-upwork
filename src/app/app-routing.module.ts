import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuardService } from './shared/services/auth-guard.service';

const routes: Routes = [{
  path: '',
  loadChildren: './home/home.module#HomeModule'
},
{
  path: 'login',
  loadChildren: './login/login.module#LoginModule'
},
{
  path: 'signup',
  loadChildren: './signup/signup.module#SignupModule'
}, {
  path: 'pages',
  loadChildren: './pages/pages.module#PagesModule',
  // canActivate: [AuthGuardService]
}];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, { useHash: false })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}