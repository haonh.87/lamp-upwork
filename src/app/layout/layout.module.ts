import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavAccountComponent } from './nav-account/nav-account.component';
import { CustomSearchModule } from '../shared/directives/custom-search/custom-search.module';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, NavbarComponent, NavAccountComponent],

  imports: [
    CommonModule,
    LayoutRoutingModule,
    CustomSearchModule
  ],

  exports: [HeaderComponent, FooterComponent, NavbarComponent, NavAccountComponent]
})
export class LayoutModule { }
