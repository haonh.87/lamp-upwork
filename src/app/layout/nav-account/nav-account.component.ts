import { AppConstants } from './../../shared/configs/constants';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-account',
  templateUrl: './nav-account.component.html',
  styleUrls: ['./nav-account.component.scss']
})
export class NavAccountComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  menus: any = [];

  ngOnInit() {
    this.menus = AppConstants.ACCOUNTS_MENU_LIST;
  }

  isActive(url: string): boolean {
    return this.router.isActive(url, true);
  }
}
