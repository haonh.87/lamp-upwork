import {Component, HostListener, OnInit, Inject} from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    @Inject(DOCUMENT) document,
  ) { }

  public show = true;
  public mbShow = true;

  @HostListener('window:scroll', ['$event'])
  onScrolling(e) {
    let body = document.body;
    let header = document.getElementById('header');

    if (window.pageYOffset > 0) {
      body.classList.add('is-scrolling');
      header.classList.add('fixed-top');
    } else {
      body.classList.remove('is-scrolling');
      header.classList.remove('fixed-top');
    }
  }

  ngOnInit() {

  }
}
