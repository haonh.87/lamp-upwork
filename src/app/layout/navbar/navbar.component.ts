import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() {
  }

  public menuMainShow = true;
  public menuLeftShow = false;
  public menuRightShow = false;

  public redirectLeft = 'l';
  public redirectRight = 'r';
  public redirectMainFLeft = 'ml';
  public redirectMainFRight = 'mr';

  ngOnInit() {

  }

  @HostListener('click', ['$event.target.dataset'])
  onMouseEnter(data: any) {
    // Redirect the elements
    if (data.redirect === this.redirectLeft) {
      this.menuMainShow = false;
      this.menuLeftShow = true;
    } else if (data.redirect === this.redirectRight) {
      this.menuMainShow = false;
      this.menuRightShow = true;
    } else if (data.redirect === this.redirectMainFLeft) {
      this.menuMainShow = true;
      this.menuLeftShow = false;
    } else if (data.redirect === this.redirectMainFRight) {
      this.menuMainShow = true;
      this.menuRightShow = false;
    }
  }
}
