import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AppConstants } from './../shared/configs/constants';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})

export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  completeForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

    emailState: boolean = false;
    completeState: boolean = true;
    showClient: boolean = false;
    showFreelancer: boolean = false;

    singleSelect: any = [];
    multiSelect: any = [];
    stringArray: any = [];
    objectsArray: any = [];

    currentCountry: any;

    config = {
        displayKey: 'name', //if objects array passed which key to be displayed defaults to description
        search: true,
        height: '200px',
        // limitTo: 3,
        icon: '<i class="fa fa-globe" aria-hidden="true"></i>'
    };


    countries:any = [];

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    ) {
        // redirect to home if already logged in
    }

    ngOnInit() {
        this.signupForm = this.formBuilder.group({
            firstname: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
            lastname: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
            email: ['', [Validators.required, Validators.email, Validators.min(6)]]
        });

        this.completeForm = this.formBuilder.group({
            country: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', [
                Validators.required,
                Validators.min(8),
                Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&].{8,}')
            ]],
            promotionalEmailOptIn: [''],
            termsAccepted: ['', Validators.required],
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.signupForm.controls;
    }

    get g() {
        return this.completeForm.controls;
    }

  onSubmit() {
    this.submitted = true;
        this.loading = true;
        this.confirmEmail();
    }

    completeSubmit() {
        this.completeState = true;
        console.log(this.completeState);
    }

    confirmEmail() {
        this.emailState = true;
        this.completeState = false;
        // return this.completeForm.controls;
    }

    toggleClient() {
        this.showFreelancer = false;
        this.showClient = true;

    }

    toggleFreelancer() {
        this.showFreelancer = true;
        this.showClient = false;
    }


}
