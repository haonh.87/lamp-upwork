import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { LayoutModule } from './../layout/layout.module';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { CustomSelectModule } from './../shared/directives/custom-select/custom-select.module';
import {RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings} from 'ng-recaptcha';
import {RecaptchaFormsModule} from 'ng-recaptcha/forms';
import { AppConstants } from './../shared/configs/constants';
import { GeolocationService } from './../shared/services/geolocation.service';

@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    ReactiveFormsModule,
    CustomSelectModule,
    SignupRoutingModule,
      RecaptchaModule,
      RecaptchaFormsModule,
  ],
    providers: [{
        provide: RECAPTCHA_SETTINGS,
        useValue: {
            siteKey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
        } as RecaptchaSettings,
    },{
      provide: AppConstants
    },{
      provide: GeolocationService,
      useClass: GeolocationService
    }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    ReactiveFormsModule
  ]
})
export class SignupModule { }
