import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule   } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LayoutModule } from './../layout/layout.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ReactiveFormsModule,
    LoginRoutingModule
  ],
  exports: [
    ReactiveFormsModule
  ]
})
export class LoginModule { }
