import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../shared/services/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    usernameForm: FormGroup;
    passwordForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    usernameState = true;
    passwordState = false;
    username = '';
    passwordIncorrect = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpClient,
        private authenticationService: AuthenticationService
    ) {
        // redirect to home if already logged in
    }

    ngOnInit() {
        this.usernameForm = this.formBuilder.group({
            username: ['', [Validators.required,
                this.emailDomainValidator]],
        });
        this.passwordForm = this.formBuilder.group({
            password: ['', Validators.required],
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.usernameForm.controls; }
    get p() { return this.passwordForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
    }

    submitUsername() {
        const {valid, value} = this.usernameForm;
        this.submitted = true;
        if (this.usernameForm.invalid) {
            return;
        }
        this.username = value.username;
        if (this.username.indexOf('@')>1) {
            this.usernameForm.registerControl(
                'username', new FormControl('', [Validators.email])
            );
        }

        // Call api check username isset
        let checkUsername = this.authenticationService.checkUsername(this.username);

        if (checkUsername) {
            this.passwordState = true;
            this.usernameState = false;
            this.submitted = false;
        }
    }

    submitLogin() {
        const {valid, value} = this.passwordForm;
        this.submitted = true;
        if (this.passwordForm.invalid) {
            return;
        }

        // Check username and password
        let checkCredential = this.authenticationService.login(this.username, value.password);

        if (checkCredential) {
            this.router.navigate(['/']);
        } else {
            this.passwordIncorrect = true;
        }

    }

    changeUser() {
        this.passwordState = false;
        this.usernameState = true;
        this.passwordIncorrect = false;
    }

    emailDomainValidator(control: FormControl) {
        let email = control.value;
        if (email.indexOf('@') != -1) {
            let valid = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{1,})$/.test(email);
            if (!valid) {
                return {
                    emailInValid: true
                }
            }
        }
        return null;
    }

}
