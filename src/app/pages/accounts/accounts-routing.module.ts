import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { 
  MyInfoComponent,
  BillingComponent,
  SecurityComponent,
  MembershipComponent } from './';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      redirectTo: 'my-info',
      pathMatch: 'full'
    },
    {
      path: 'my-info',
      component: MyInfoComponent,
      pathMatch: 'full'
    },
    {
      path: 'billing',
      component: BillingComponent,
      pathMatch: 'full'
    },
    {
      path: 'password-and-sercurity',
      component: SecurityComponent,
      pathMatch: 'full'
    },
    {
      path: 'membership',
      component: MembershipComponent,
      pathMatch: 'full'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
