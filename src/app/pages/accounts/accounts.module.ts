import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AccountsRoutingModule } from './accounts-routing.module';
import { MyInfoComponent } from './my-info/my-info.component';
import { LayoutModule } from './../../layout/layout.module';
import { BillingComponent } from './billing/billing.component';
import { SecurityComponent } from './security/security.component';
import { MembershipComponent } from './membership/membership.component';

@NgModule({
  declarations: [MyInfoComponent, BillingComponent, SecurityComponent, MembershipComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    ReactiveFormsModule,
    AccountsRoutingModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    ReactiveFormsModule
  ]
})
export class AccountsModule { }
