import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { AccountsModule } from './accounts/accounts.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccountsModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
